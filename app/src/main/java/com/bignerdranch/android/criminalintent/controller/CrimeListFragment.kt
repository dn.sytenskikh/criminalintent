package com.bignerdranch.android.criminalintent.controller

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bignerdranch.android.criminalintent.R
import com.bignerdranch.android.criminalintent.model.Crime
import com.bignerdranch.android.criminalintent.model.CrimeLab
import kotlinx.android.synthetic.main.fragment_crime_list.view.recycler_view_crime_list
import kotlinx.android.synthetic.main.item_crime_list.view.*
import java.text.DateFormat
import java.util.zip.DataFormatException


class CrimeListFragment : Fragment() {

    private var adapter: CrimeAdapter? = null
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime_list, container, false)
        recyclerView = view.recycler_view_crime_list
        recyclerView.layoutManager = LinearLayoutManager(activity)

        updateUI()

        return view
    }

    override fun onResume() {
        super.onResume()

        updateUI()
    }


    private inner class CrimeHolder(inflater: LayoutInflater, parent: ViewGroup?) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_crime_list, parent, false)),
        View.OnClickListener {

        private lateinit var mCrime: Crime

        fun bind(crime: Crime) {
            mCrime = crime
            itemView.apply {
                text_view_crime_title.text = crime.title
                text_view_crime_date.text = DateFormat.getDateTimeInstance().format(crime.date)
                image_view_crime_solved.visibility = if (crime.solved) View.VISIBLE else View.GONE
            }

            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            val intent = CrimePagerActivity.newIntent(activity as FragmentActivity, mCrime.id)
            startActivity(intent)
        }
    }

    private inner class CrimeAdapter(val crimes: MutableList<Crime>) :
        RecyclerView.Adapter<CrimeHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CrimeHolder {
            val layoutInflater = LayoutInflater.from(activity)
            return CrimeHolder(layoutInflater, parent)
        }

        override fun getItemCount(): Int {
            return crimes.count()
        }

        override fun onBindViewHolder(holder: CrimeHolder, position: Int) {
            val crime = crimes[position]
            holder.bind(crime)
        }
    }

    private fun updateUI() {
        val crimes = CrimeLab.getCrimes()

        if (adapter == null) {
            adapter = CrimeAdapter(crimes)
            recyclerView.adapter = adapter
        } else {
            adapter?.notifyDataSetChanged()
        }
    }
}