package com.bignerdranch.android.criminalintent.model

import java.util.*

data class Crime(var title: String, var solved: Boolean) {
    val id: UUID = UUID.randomUUID()
    var date: Date = Date(100000L)
}