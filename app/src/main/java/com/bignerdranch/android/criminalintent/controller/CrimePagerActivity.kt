package com.bignerdranch.android.criminalintent.controller

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bignerdranch.android.criminalintent.R
import com.bignerdranch.android.criminalintent.model.CrimeLab
import kotlinx.android.synthetic.main.activity_crime_pager.*
import java.util.*


class CrimePagerActivity: AppCompatActivity() {

    companion object {
        private const val EXTRA_CRIME_ID = "com.bignerdranch.android.criminalintent.crime_id"

        fun newIntent(packageActivity: Activity, crimeID: UUID): Intent {
            val intent = Intent(packageActivity, CrimePagerActivity::class.java)
            intent.putExtra(EXTRA_CRIME_ID, crimeID)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crime_pager)

        val crimeId = intent.getSerializableExtra(EXTRA_CRIME_ID) as UUID

        val crimes = CrimeLab.getCrimes()


        view_pager_crime.adapter = object: FragmentStatePagerAdapter(supportFragmentManager){
            override fun getItem(position: Int): Fragment = CrimeFragment.newInstance(crimes[position].id)
            override fun getCount(): Int = crimes.size
        }

        for (i in 0 until crimes.size) {
            if (crimes[i].id == crimeId) {
                view_pager_crime.currentItem = i
                break
            }
        }
    }
}