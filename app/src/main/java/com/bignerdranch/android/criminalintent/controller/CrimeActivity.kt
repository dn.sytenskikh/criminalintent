package com.bignerdranch.android.criminalintent.controller

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import com.bignerdranch.android.criminalintent.SingleFragmentActivity
import java.util.*


class CrimeActivity : SingleFragmentActivity() {
    companion object {
        private const val EXTRA_CRIME_ID = "com.bignerdranch.android.criminalintent.crime_id"

        fun newIntent(packageActivity: Activity, crimeID: UUID): Intent {
            val intent = Intent(packageActivity, CrimeActivity::class.java)
            intent.putExtra(EXTRA_CRIME_ID, crimeID)
            return intent
        }
    }

    override fun createFragment(): Fragment {
        val crimeId = intent.getSerializableExtra(EXTRA_CRIME_ID) as UUID
        return CrimeFragment.newInstance(crimeId)
    }
}
