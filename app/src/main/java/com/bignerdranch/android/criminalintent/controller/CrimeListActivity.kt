package com.bignerdranch.android.criminalintent.controller

import androidx.fragment.app.Fragment
import com.bignerdranch.android.criminalintent.SingleFragmentActivity


class CrimeListActivity: SingleFragmentActivity() {
    override fun createFragment(): Fragment {
        return CrimeListFragment()
    }
}