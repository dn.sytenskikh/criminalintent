package com.bignerdranch.android.criminalintent.controller

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import com.bignerdranch.android.criminalintent.R
import com.bignerdranch.android.criminalintent.model.Crime
import com.bignerdranch.android.criminalintent.model.CrimeLab
import kotlinx.android.synthetic.main.fragment_crime.view.*
import java.text.DateFormat
import java.util.*


class CrimeFragment : Fragment() {

    companion object {
        private const val ARG_CRIME_ID = "crime_id"
        private const val DIALOG_DATE = "DialogDate"

        fun newInstance(crimeId: UUID): CrimeFragment {
            val args = Bundle()
            args.putSerializable(ARG_CRIME_ID, crimeId)

            val fragment = CrimeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var crime: Crime

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val crimeId = arguments?.getSerializable(ARG_CRIME_ID) as UUID
        crime = CrimeLab.getCrime(crimeId)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime, container, false)

        view.edit_text_crime_title.hint = crime.title
        view.edit_text_crime_title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                crime.title = s.toString()
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        view.button_crime_date.text = DateFormat.getDateTimeInstance().format(crime.date)
        view.button_crime_date.setOnClickListener {
            val dialog = DatePickerFragment.newInstance(crime.date)

            fragmentManager?.let { dialog.show(it, DIALOG_DATE) }
        }

        view.check_box_crime_solved.isChecked = crime.solved
        view.check_box_crime_solved.setOnCheckedChangeListener { buttonView: CompoundButton, isChecked: Boolean ->
            crime.solved = isChecked
        }

        return view
    }
}