package com.bignerdranch.android.criminalintent.model

import java.util.*

object CrimeLab {
    private val crimes = mutableListOf<Crime>()
    private val crimesDict = hashMapOf<UUID, Crime>()

    init {
        for (i in 0..99) {
            val crime = Crime("Crime #$i", i % 2 == 0)
            crimes.add(crime)

            crimesDict[crime.id] = crime
        }


    }

    fun getCrimes(): MutableList<Crime> {
        return crimes
    }

    fun getCrime(uuid: UUID): Crime? = crimesDict[uuid]
}